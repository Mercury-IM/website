---
title: "Contribute"
date: 2019-09-07T01:25:37+02:00
draft: true
---

You can help improving Mercury IM!

## Translate

Mercury IM is not available in your language? Change that by contributing translations!

TODO: Translation Platform

## Design

Don't like the current launcher icon? Make a new one! If we like it, we may use it!

TODO: More information

## Code

Join us over on [Codeberg.org](https://codeberg.org/Mercury-IM/) where Mercury IM is being developed.

TODO: More information
