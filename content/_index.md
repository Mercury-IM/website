---
title: "Mercury IM - A Freedom Respecting Messenger"
date: 2019-09-06
draft: false
---

Mercury IM began as a small hobby project. The goal was to see how easy it would be to create a fully functioning messaging client for Android.

## Guiding Principles

You have the right to freedom! Mercury IM empowers you by granting you the 4 freedoms of free software: The right to *use*, *study*, *share* and *improve* it.

Thats why Mercury IM is licensed under the [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html) or later to guarantee that it will forever be free software that respects your rights.

Furthermore Mercury IM does not try to lock you in in any way. If you don't like the app you deserve to be able to use something else without losing all your contacts and messages.

For that reason Mercury IM did not reinvent the wheel but is build on top of the openly developed [XMPP protocol standard](https://xmpp.org/). That way the inner workings of the app are not kept a secret and everybody is able to build a compatible app to join the conversation. In fact, there is a wide range of [compatible messenger apps](https://xmpp.org/software/clients.html) for many platforms out there already!

# Coming Soon!

Mercury IM is still in the earliest state of development, so you gonna have to wait a little longer :)
Soon though the repository will be made public so that anyone can join the community!

